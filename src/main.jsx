import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import Routes from "./routes.jsx";
import Header from "./components/header/header.jsx";
import { BrowserRouter } from "react-router-dom";
import { ChakraProvider } from "@chakra-ui/react";
ReactDOM.createRoot(document.getElementById("root")).render(
  <BrowserRouter>
    <ChakraProvider>
      <Header />
      <Routes />
    </ChakraProvider>
  </BrowserRouter>
);
