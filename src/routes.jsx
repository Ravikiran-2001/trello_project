import React from "react";
import App from "./App";
import BoardLists from "./components/boardLists/boardLists";
import { Routes, Route } from "react-router-dom";

const routes = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="/boards/:id" element={<BoardLists />} />
      </Routes>
    </>
  );
};

export default routes;
