import React, { useEffect, useState } from "react";
import {
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  Box,
  Flex,
  Input,
  Spinner,
} from "@chakra-ui/react";
import { DeleteIcon } from "@chakra-ui/icons";
import {
  getChecklists,
  createChecklist,
  deleteChecklist,
} from "../../api/addCheckListApi";
import CheckItems from "../checkItems/checkItems";

const AddCheckList = ({ cardsId, onClose }) => {
  const [allChecklists, setAllChecklists] = useState([]);
  const [newListName, setNewListName] = useState("");
  const [isCreatingChecklist, setIsCreatingChecklist] = useState(false);
  const [isError, setIsError] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    getChecklists(cardsId)
      .then((res) => {
        setAllChecklists(res);
        setLoading(false);
      })
      .catch((error) => {
        setIsError(true);
        setLoading(false);
      });
  }, []);

  const handleCloseModal = () => {
    onClose();
    setNewListName("");
  };

  const handleCreateChecklist = (event) => {
    setLoading(true);
    event.preventDefault();
    createChecklist(cardsId, newListName)
      .then((res) => {
        setAllChecklists([...allChecklists, res]);
        setIsCreatingChecklist(false);
        setNewListName("");
        setLoading(false);
      })
      .catch((error) => {
        setIsError(true);
        setLoading(false);
      });
  };

  const handleDeleteChecklist = (checkListId) => {
    setLoading(true);
    deleteChecklist(cardsId, checkListId)
      .then(() => {
        setAllChecklists((prevLists) =>
          prevLists.filter((checklist) => checklist.id !== checkListId)
        );
        setLoading(false);
      })
      .catch((error) => {
        setIsError(true);
        setLoading(false);
      });
  };

  return (
    <div>
      <Modal isOpen={true} onClose={handleCloseModal}>
        <ModalOverlay />
        <ModalContent>
          {isError ? (
            <Box p="4" bg="red.200" color="red.900" borderRadius="md">
              An error occurred.
            </Box>
          ) : (
            <>
              {loading ? (
                <Flex justify="center" align="center" bg="gray.100" p="4">
                  <Spinner size="lg" color="blue.500" />
                </Flex>
              ) : (
                allChecklists.map((checklist) => (
                  <Box
                    key={checklist.id}
                    bg="grey"
                    borderRadius="md"
                    p="1"
                    m="1"
                    color="white"
                    cursor="pointer"
                  >
                    <Flex justifyContent="space-between" alignItems="center">
                      <Box fontWeight="bold" fontSize="xl" color="black">
                        {checklist.name}
                      </Box>
                      <Button
                        bg="black"
                        color="white"
                        onClick={() => handleDeleteChecklist(checklist.id)}
                      >
                        <DeleteIcon />
                      </Button>
                    </Flex>
                    <CheckItems checkListId={checklist.id} cardsId={cardsId} />
                  </Box>
                ))
              )}
              {isCreatingChecklist ? (
                <form onSubmit={handleCreateChecklist}>
                  <Flex flexDirection="column" gap="2">
                    <Input
                      placeholder="Enter checklist name"
                      value={newListName}
                      onChange={(e) => setNewListName(e.target.value)}
                    />
                    <Flex justifyContent="space-between">
                      <Button type="submit">Add</Button>
                      <Button onClick={() => setIsCreatingChecklist(false)}>
                        Cancel
                      </Button>
                    </Flex>
                  </Flex>
                </form>
              ) : (
                <Button
                  bg="black"
                  color="white"
                  onClick={() => setIsCreatingChecklist(true)}
                >
                  Create checklist
                </Button>
              )}
            </>
          )}
        </ModalContent>
      </Modal>
    </div>
  );
};

export default AddCheckList;
