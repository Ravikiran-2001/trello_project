import React, { useEffect, useState } from "react";
import { Box, Flex, Input, Button, Spinner } from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";
import AddCheckList from "../checkList/addCheckList";
import { getAllCards, createCard, deleteCard } from "../../api/allCardsApi";

const AllCards = ({ listId }) => {
  const [cards, setCards] = useState([]);
  const [isAddingCard, setIsAddingCard] = useState(false);
  const [newCardName, setNewCardName] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [selectedCardId, setSelectedCardId] = useState(null);
  const [loading, setLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  useEffect(() => {
    setLoading(true);
    getAllCards(listId)
      .then((data) => {
        setCards(data);
        setLoading(false);
      })
      .catch((error) => {
        setIsError(true);
        setLoading(false);
      });
  }, []);

  const handleCreateCard = (event) => {
    event.preventDefault();
    setLoading(true);
    createCard(listId, newCardName)
      .then((res) => {
        setCards([...cards, res]);
        setIsAddingCard(false);
        setNewCardName("");
        setLoading(false);
      })
      .catch((error) => {
        setIsError(true);
        setLoading(false);
      });
  };

  const handleDeleteCard = (cardId) => {
    setLoading(true);
    deleteCard(cardId)
      .then(() => {
        setCards((prevCards) => prevCards.filter((card) => card.id !== cardId));
        setLoading(false);
      })
      .catch((error) => {
        setIsError(true);
        setLoading(false);
      });
  };

  const handleAddCheckList = (cardId) => {
    setShowModal(true);
    setSelectedCardId(cardId);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  return (
    <>
      {isError ? (
        <Box p="4" bg="red.200" color="red.900" borderRadius="md">
          An error occurred.
        </Box>
      ) : (
        <div>
          {cards.map((card) => (
            <Box
              key={card.id}
              bg="grey"
              w="11rem"
              borderRadius="md"
              p="1"
              m="1"
              color="white"
              cursor="pointer"
            >
              {loading ? (
                <Flex justify="center" align="center" bg="grey">
                  <Spinner
                    size="xl"
                    color="blue.500"
                    thickness="4px"
                    speed="0.65s"
                  />
                </Flex>
              ) : (
                <Flex justifyContent={"space-between"}>
                  <div onClick={() => handleAddCheckList(card.id)}>
                    {card.name}
                  </div>

                  <button onClick={() => handleDeleteCard(card.id)}>
                    <CloseIcon />
                  </button>
                </Flex>
              )}
            </Box>
          ))}

          {isAddingCard ? (
            <form onSubmit={handleCreateCard}>
              <Flex flexDirection="column">
                <Input
                  placeholder="Enter Card name"
                  value={newCardName}
                  onChange={(event) => setNewCardName(event.target.value)}
                />
                <Flex justifyContent="space-between" mt="1">
                  <Button type="submit" p="1">
                    Add
                  </Button>
                  <Button
                    type="button"
                    p="1"
                    onClick={() => setIsAddingCard(false)}
                  >
                    Cancel
                  </Button>
                </Flex>
              </Flex>
            </form>
          ) : (
            <button onClick={() => setIsAddingCard(true)}>+ Add a card</button>
          )}
          {showModal && (
            <AddCheckList cardsId={selectedCardId} onClose={handleCloseModal} />
          )}
        </div>
      )}
    </>
  );
};

export default AllCards;
