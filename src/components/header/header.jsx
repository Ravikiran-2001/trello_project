import React from "react";
import { Box, Flex } from "@chakra-ui/react";
import { Link } from "react-router-dom";
const Header = () => {
  return (
    <Box bg="rgb(8, 117, 184)" py="4">
      <Flex align="center" px="4" gap="5rem">
        <Link to="/">
          <img
            src="https://trello.com/assets/d947df93bc055849898e.gif"
            alt="trello-logo"
            width="100"
            height="auto"
          />
        </Link>
      </Flex>
    </Box>
  );
};

export default Header;
