import React, { useEffect, useState } from "react";
import {
  Box,
  Flex,
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Input,
  Spinner,
} from "@chakra-ui/react";
import { DeleteIcon } from "@chakra-ui/icons";
import { useParams } from "react-router";
import { getBoardLists, createList, deleteList } from "../../api/boardListApi";
import AllCards from "../cards/allCards";

const BoardLists = () => {
  const { id } = useParams();
  const [data, setData] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [newListName, setNewListName] = useState("");
  const [loading, setLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  useEffect(() => {
    setLoading(true);
    getBoardLists(id)
      .then((lists) => {
        setData(lists);
        setLoading(false);
      })
      .catch((error) => {
        setIsError(true);
      });
  }, []);

  const handleAddList = () => {
    setShowModal(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
    setNewListName("");
  };

  const handleCreateList = (event) => {
    setLoading(true);
    event.preventDefault();
    createList(id, newListName)
      .then((newList) => {
        setData([...data, newList]);
        setLoading(false);
        setShowModal(false);
        setNewListName("");
      })
      .catch((error) => {
        setIsError(true);
      });
  };

  const handleDelete = (listId) => {
    setLoading(true);
    deleteList(listId)
      .then(() => {
        const newList = data.filter((list) => list.id !== listId);
        setLoading(false);
        setData(newList);
      })
      .catch((error) => {
        setIsError(true);
      });
  };

  return (
    <>
      {isError ? (
        <Box p="4" bg="red.200" color="red.900" borderRadius="md">
          An error occurred.
        </Box>
      ) : (
        <>
          {loading ? (
            <Flex
              width="100vw"
              height="100vh"
              justify="center"
              align="center"
              bg="white"
            >
              <Spinner
                size="xl"
                color="blue.500"
                thickness="4px"
                speed="0.65s"
              />
            </Flex>
          ) : (
            <div>
              <Flex flexWrap="wrap">
                {data.map((lists) => (
                  <Box
                    key={lists.id}
                    p="2"
                    mb="2"
                    borderWidth="1px"
                    borderRadius="md"
                    width="200px"
                    display="flex"
                    flexDirection="column"
                    gap="2rem"
                    alignItems="center"
                    justifyContent="center"
                    bg="rgb(7, 86, 135)"
                    color="white"
                    m="4"
                  >
                    <h2>{lists.name}</h2>
                    <AllCards listId={lists.id} />
                    <Flex justifyContent="space-between" gap="4.5rem">
                      <button onClick={() => handleDelete(lists.id)}>
                        <DeleteIcon />
                      </button>
                    </Flex>
                  </Box>
                ))}
                <Button m="4" onClick={handleAddList}>
                  + add another list
                </Button>
              </Flex>
              <Modal isOpen={showModal} onClose={handleCloseModal}>
                <ModalOverlay />
                <ModalContent>
                  <ModalHeader>Add New List</ModalHeader>
                  <form onSubmit={(event) => handleCreateList(event)}>
                    <ModalBody>
                      <Input
                        placeholder="Enter list name"
                        value={newListName}
                        onChange={(event) => setNewListName(event.target.value)}
                      />
                    </ModalBody>
                    <ModalFooter>
                      <Button type="submit" colorScheme="teal" mr={3}>
                        Add
                      </Button>
                      <Button variant="ghost" onClick={handleCloseModal}>
                        Cancel
                      </Button>
                    </ModalFooter>
                  </form>
                </ModalContent>
              </Modal>
            </div>
          )}
        </>
      )}
    </>
  );
};

export default BoardLists;
