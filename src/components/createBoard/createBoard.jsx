import React, { useState } from "react";
import { createBoard } from "../../api/createBoardApi";
import {
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  Input,
  Box,
} from "@chakra-ui/react";

const CreateBoard = ({ onCreateBoard }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [newBoardName, setNewBoardName] = useState("");
  const [isError, setIsError] = useState(false);

  const handleClick = () => {
    setIsOpen(true);
  };

  const handleClose = () => {
    setIsOpen(false);
  };

  const handleInputChange = (event) => {
    setNewBoardName(event.target.value);
  };

  const handleCreateBoard = (event) => {
    event.preventDefault();
    createBoard(newBoardName)
      .then((res) => {
        onCreateBoard(res);
        setIsError(false);
      })
      .catch((error) => {
        setIsError(true);
      });

    setNewBoardName("");
    setIsOpen(false);
  };

  return (
    <>
      {isError ? (
        <Box p="4" bg="red.200" color="red.900" borderRadius="md">
          An error occurred.
        </Box>
      ) : (
        <>
          <Button
            colorScheme="rgb(84, 8, 184)"
            variant="outline"
            onClick={handleClick}
            borderRadius="md"
            width="150px"
            height="100px"
            display="flex"
            alignItems="center"
            justifyContent="center"
            m="4"
          >
            + Create
          </Button>

          <Modal isOpen={isOpen} onClose={handleClose}>
            <ModalOverlay />
            <ModalContent>
              <ModalHeader>Create Board</ModalHeader>
              <ModalCloseButton />
              <form onSubmit={handleCreateBoard}>
                <ModalBody>
                  <Input
                    placeholder="Board Name"
                    value={newBoardName}
                    onChange={handleInputChange}
                  />
                </ModalBody>

                <ModalFooter>
                  <Button type="submit" colorScheme="blue" mr={3}>
                    Create
                  </Button>
                  <Button variant="ghost" onClick={handleClose}>
                    Cancel
                  </Button>
                </ModalFooter>
              </form>
            </ModalContent>
          </Modal>
        </>
      )}
    </>
  );
};

export default CreateBoard;
