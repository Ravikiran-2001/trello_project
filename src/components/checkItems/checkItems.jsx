import React, { useEffect, useState } from "react";
import {
  Box,
  Button,
  Flex,
  Input,
  Progress,
  Checkbox,
  Spinner,
} from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";
import {
  getCheckItems,
  createCheckItem,
  deleteCheckItem,
  updateCheckItem,
} from "../../api/checkItemsApi";

const CheckItems = ({ checkListId, cardsId }) => {
  const [allCheckItems, setAllCheckItems] = useState([]);
  const [isNewItem, setIsNewItem] = useState(false);
  const [newItem, setNewItem] = useState("");
  const [isError, setIsError] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    getCheckItems(checkListId)
      .then((data) => {
        setAllCheckItems(data);
        setLoading(false);
      })
      .catch((error) => {
        setIsError(true);
        setLoading(false);
      });
  }, []);

  const handleCreateCheckItems = (event) => {
    setLoading(true);
    event.preventDefault();
    createCheckItem(checkListId, newItem)
      .then((res) => {
        setAllCheckItems([...allCheckItems, res]);
        setIsNewItem(false);
        setNewItem("");
        setLoading(false);
      })
      .catch((error) => {
        setIsError(true);
        setLoading(false);
      });
  };

  const handleDeleteItems = (itemId) => {
    setLoading(true);
    deleteCheckItem(checkListId, itemId)
      .then(() => {
        setAllCheckItems((prevItems) =>
          prevItems.filter((item) => item.id !== itemId)
        );
        setLoading(false);
      })
      .catch((error) => {
        setIsError(true);
        setLoading(false);
      });
  };

  const handleToggleCheckBox = (itemId) => {
    const updatedItems = allCheckItems.map((item) =>
      item.id === itemId
        ? {
            ...item,
            state: item.state === "complete" ? "incomplete" : "complete",
          }
        : item
    );
    setAllCheckItems(updatedItems);
    updateCheckItem(cardsId, checkListId, itemId, updatedItems);
  };

  return (
    <div>
      {isError ? (
        <Box p="4" bg="red.200" color="red.900" borderRadius="md">
          An error occurred.
        </Box>
      ) : (
        <>
          <Progress
            mt="1"
            colorScheme="blue"
            hasStripe
            value={
              allCheckItems.length > 0
                ? (allCheckItems.filter((item) => item.state === "complete")
                    .length /
                    allCheckItems.length) *
                  100
                : 0
            }
          />
          {allCheckItems.map((item) => (
            <Box
              key={item.id}
              bg="black"
              w="20rem"
              borderRadius="md"
              p="1"
              m="2"
              color="white"
              cursor="pointer"
            >
              {loading ? (
                <Flex justify="center" align="center" bg="grey">
                  <Spinner
                    size="xl"
                    color="blue.500"
                    thickness="4px"
                    speed="0.65s"
                  />
                </Flex>
              ) : (
                <Flex justifyContent="space-between" alignItems="center" pl="2">
                  <Checkbox
                    isChecked={item.state === "complete"}
                    onChange={() => handleToggleCheckBox(item.id)}
                  >
                    {item.name}
                  </Checkbox>
                  <Button
                    bg="black"
                    color="white"
                    onClick={() => handleDeleteItems(item.id)}
                  >
                    <CloseIcon />
                  </Button>
                </Flex>
              )}
            </Box>
          ))}
          {isNewItem ? (
            <form onSubmit={handleCreateCheckItems}>
              <Flex flexDirection="column" gap="2" mt="1">
                <Input
                  color="white"
                  value={newItem}
                  onChange={(e) => setNewItem(e.target.value)}
                />
                <Flex justifyContent="space-between">
                  <Button type="submit" p="1">
                    Add
                  </Button>
                  <Button p="1" onClick={() => setIsNewItem(false)}>
                    Cancel
                  </Button>
                </Flex>
              </Flex>
            </form>
          ) : (
            <Button mt="1" onClick={() => setIsNewItem(true)}>
              Add item
            </Button>
          )}
        </>
      )}
    </div>
  );
};

export default CheckItems;
