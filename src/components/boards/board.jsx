import React from "react";
import { useNavigate } from "react-router-dom";
import { Box, Flex } from "@chakra-ui/react";

const Board = ({ data, isError }) => {
  const navigate = useNavigate();
  return (
    <div>
      {isError ? (
        <Box p="4" bg="red.200" color="red.900" borderRadius="md">
          An error occurred.
        </Box>
      ) : (
        <>
          <Flex flexWrap="wrap">
            {data &&
              data.map((board) => (
                <Box
                  key={board.id}
                  m="4"
                  borderWidth="1px"
                  borderRadius="md"
                  width="150px"
                  height="100px"
                  display="flex"
                  alignItems="center"
                  justifyContent="center"
                  color="white"
                  bg="rgb(7, 86, 135)"
                  cursor="pointer"
                  onClick={() => navigate(`/boards/${board.id}`)}
                >
                  <h2>{board.name} </h2>
                </Box>
              ))}
          </Flex>
        </>
      )}
    </div>
  );
};

export default Board;
