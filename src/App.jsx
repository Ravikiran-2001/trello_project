import React, { useState, useEffect } from "react";
import { Heading, Spinner, Flex, Box } from "@chakra-ui/react";
import axiosInstance from "./axios/axiosInstance";
import "./App.css";
import Board from "./components/boards/board";
import CreateBoard from "./components/createBoard/createBoard";

function App() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [isError, setIsError] = useState(false);

  useEffect(() => {
    setLoading(true);
    axiosInstance
      .get(`members/me/boards`)
      .then((res) => {
        setData(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("error is fetching :", error);
        setLoading(false);
        setIsError(true);
      });
  }, []);

  const handleCreateBoard = (newBoard) => {
    setData([...data, newBoard]);
  };

  return (
    <>
      {isError ? (
        <Box p="4" bg="red.200" color="red.900" borderRadius="md">
          An error occurred.
        </Box>
      ) : (
        <>
          {loading ? (
            <Flex
              width="100vw"
              height="100vh"
              justify="center"
              align="center"
              bg="white"
            >
              <Spinner
                size="xl"
                color="blue.500"
                thickness="4px"
                speed="0.65s"
              />
            </Flex>
          ) : (
            <>
              <Heading color="rgb(56, 53, 53)" ml="4" mt="4">
                Your work space
              </Heading>
              <Board data={data} isError={isError} />
              <CreateBoard onCreateBoard={handleCreateBoard} />
            </>
          )}
        </>
      )}
    </>
  );
}

export default App;
