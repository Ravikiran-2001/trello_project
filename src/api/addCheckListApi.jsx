import axiosInstance from "../axios/axiosInstance";

const getChecklists = (cardsId) => {
  return axiosInstance
    .get(`cards/${cardsId}/checklists`)
    .then((res) => res.data)
    .catch((error) => {
      console.error("Error fetching checklists:", error);
      throw error;
    });
};

const createChecklist = (cardsId, newName) => {
  return axiosInstance
    .post(`cards/${cardsId}/checklists`, { name: newName })
    .then((res) => res.data)
    .catch((error) => {
      console.error("Error creating checklist:", error);
      throw error;
    });
};

const deleteChecklist = (cardsId, checkListId) => {
  return axiosInstance
    .delete(`cards/${cardsId}/checklists/${checkListId}`)
    .catch((error) => {
      console.error("Error deleting checklist:", error);
      throw error;
    });
};

export { getChecklists, createChecklist, deleteChecklist };
