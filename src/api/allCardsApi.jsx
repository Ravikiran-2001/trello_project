import axiosInstance from "../axios/axiosInstance";

const getAllCards = (listId) => {
  return axiosInstance
    .get(`lists/${listId}/cards`)
    .then((res) => res.data)
    .catch((error) => {
      console.error("Error fetching cards:", error);
      throw error;
    });
};

const createCard = (listId, newCardName) => {
  return axiosInstance
    .post(`cards`, { idList: listId, name: newCardName })
    .then((res) => res.data)
    .catch((error) => {
      console.error("Error creating card:", error);
      throw error;
    });
};

const deleteCard = (cardId) => {
  return axiosInstance
    .delete(`cards/${cardId}`)
    .then(() => cardId)
    .catch((error) => {
      console.error("Error deleting card:", error);
      throw error;
    });
};

export { getAllCards, createCard, deleteCard };
