import axiosInstance from "../axios/axiosInstance";

const getBoardLists = (boardId) => {
  return axiosInstance
    .get(`boards/${boardId}/lists`)
    .then((response) => response.data)
    .catch((error) => {
      console.error("Error fetching board lists:", error);
      throw error;
    });
};

const createList = (boardId, newListName) => {
  return axiosInstance
    .post(`lists`, { name: newListName, idBoard: boardId })
    .then((response) => response.data)
    .catch((error) => {
      console.error("Error creating list:", error);
      throw error;
    });
};

const deleteList = (listId) => {
  return axiosInstance
    .put(`lists/${listId}`, { closed: true })
    .catch((error) => {
      console.error("Error deleting list:", error);
      throw error;
    });
};

export { getBoardLists, createList, deleteList };
