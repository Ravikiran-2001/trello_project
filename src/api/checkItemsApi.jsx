import axiosInstance from "../axios/axiosInstance";

const getCheckItems = (checkListId) => {
  return axiosInstance
    .get(`checklists/${checkListId}/checkItems`)
    .then((res) => res.data)
    .catch((error) => {
      console.error("Error fetching check items:", error);
      throw error;
    });
};

const createCheckItem = (checkListId, newItem) => {
  return axiosInstance
    .post(`checklists/${checkListId}/checkItems`, { name: newItem })
    .then((res) => res.data)
    .catch((error) => {
      console.error("Error creating check item:", error);
      throw error;
    });
};

const deleteCheckItem = (checkListId, itemId) => {
  return axiosInstance
    .delete(`checklists/${checkListId}/checkItems/${itemId}`)
    .then(() => itemId)
    .catch((error) => {
      console.error("Error deleting check item:", error);
      throw error;
    });
};

const updateCheckItem = (cardsId, checkListId, itemId, updatedItems) => {
  return axiosInstance
    .put(`cards/${cardsId}/checklist/${checkListId}/checkItem/${itemId}`, {
      state: updatedItems.find((item) => item.id === itemId).state,
    })
    .catch((error) => {
      console.log("Error in updating check item:", error);
    });
};

export { getCheckItems, createCheckItem, deleteCheckItem, updateCheckItem };
