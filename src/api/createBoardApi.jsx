import axiosInstance from "../axios/axiosInstance";

export const createBoard = (boardName) => {
  return axiosInstance
    .post(`boards/?name=${boardName}`)
    .then((res) => res.data)
    .catch((error) => {
      console.error("Error creating board:", error);
      throw error;
    });
};
